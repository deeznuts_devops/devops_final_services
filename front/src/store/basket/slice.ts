import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { TBasket, TProduct } from '@/models/Product';
import { TResponse } from '@/models/Status';
// import { getUserInfo, getSchedule, getUsers, editUser, deleteUser, addReceiver, addManager } from './thunk';

const initialState: TBasket = {
  items: [],
  isRead: true,
  isOpen: false,
}

const slice = createSlice({
  name: 'basket',
  initialState,
  reducers: {
    addItem(state, action: PayloadAction<TProduct>) {
      const index = state.items?.findIndex((product) => product.product.id == action.payload.id);
      if(index >= 0) {
        state.items[index].quantity++;
      } else {
        state.items?.push({
          product: action.payload,
          quantity: 1,
        });
      }
      state.isRead = false;
    },
    removeItem(state, action: PayloadAction<string>) {
      const index = state.items?.findIndex((product) => product.product.id == action.payload);
      if(index >= 0) {
        if(state.items[index].quantity >= 2) {
          state.items[index].quantity--;
        } else {
          state.items = state.items.filter((product) => product.product.id != action.payload);
        }
      }
      if(state.items.length <= 0) state.isRead = true;
    },
    openChange(state) {
      state.isOpen = !state.isOpen;
    },
  },
  extraReducers: (builder) => {
    
  },
});

export const basketReducer = slice.reducer;
export const basketActions = slice.actions;