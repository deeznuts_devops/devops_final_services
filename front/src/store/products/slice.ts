import { createSlice } from '@reduxjs/toolkit';
import { TProduct } from '@/models/Product';
import { TResponse } from '@/models/Status';
// import { getUserInfo, getSchedule, getUsers, editUser, deleteUser, addReceiver, addManager } from './thunk';

interface TProductsState {
  products?: TProduct[];
  productsResponse: TResponse;
}

const initialState: TProductsState = {
  products: [
    {
      id: "1",
      name: "Қазы",
      price: 400,
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia minima cupiditate doloribus laboriosam voluptas dicta ullam temporibus rerum recusandae exercitationem sint id, possimus debitis magni sunt! Numquam ducimus eveniet cumque.",
      categories: ["Мясо", "Молочные продукты"],
      photo: "/images/img1.jpg",
    },
    {
      id: "2",
      name: "Қазы высококачественный же есть",
      price: 9900,
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia minima cupiditate doloribus laboriosam voluptas dicta ullam temporibus rerum recusandae exercitationem sint id, possimus debitis magni sunt! Numquam ducimus eveniet cumque.",
      categories: ["Мясо", "Молочные продукты"],
      photo: "/images/img1.jpg",
    },
    {
      id: "3",
      name: "Қазы красивый",
      price: 600,
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia minima cupiditate doloribus laboriosam voluptas dicta ullam temporibus rerum recusandae exercitationem sint id, possimus debitis magni sunt! Numquam ducimus eveniet cumque.",
      categories: ["Мясо", "Молочные продукты"],
      photo: "/images/img1.jpg",
    },
    {
      id: "4",
      name: "Қазы красивый",
      price: 600,
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia minima cupiditate doloribus laboriosam voluptas dicta ullam temporibus rerum recusandae exercitationem sint id, possimus debitis magni sunt! Numquam ducimus eveniet cumque.",
      categories: ["Мясо", "Молочные продукты"],
      photo: "/images/img1.jpg",
    },
    {
      id: "5",
      name: "Қазы красивый",
      price: 600,
      description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia minima cupiditate doloribus laboriosam voluptas dicta ullam temporibus rerum recusandae exercitationem sint id, possimus debitis magni sunt! Numquam ducimus eveniet cumque.",
      categories: ["Мясо", "Молочные продукты"],
      photo: "/images/img1.jpg",
    },
  ],
  productsResponse: {
    status: 'IDLE',
  }
}

const slice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    // resetUser(state) {
    //   state.user = undefined;
    //   state.userResponse.status = "IDLE";
    // },
    // resetUsers(state) {
    //   state.usersResponse.status = "IDLE";
    //   state.usersResponse.data = undefined;
    //   state.usersResponse.message = undefined;
    // },
    // softResetEditResponse(state) {
    //   state.editResponse.message = undefined;
    //   state.editResponse.status = "IDLE";
    // }
  },
  extraReducers: (builder) => {
    // builder.addCase(getUserInfo.pending, (state) => {
    //   state.userResponse.status = "PENDING";
    // });
    // builder.addCase(getUserInfo.rejected, (state, action) => {
    //   state.userResponse.message = action.payload?.message ? action.payload.message : "Что-то пошло не так(";
    //   state.userResponse.status = "FAILED";
    // });
    // builder.addCase(getUserInfo.fulfilled, (state, action) => {
    //   state.userResponse.status = "SUCCEEDED";
    //   state.user = action.payload.data.user;
    // });

    // builder.addCase(getSchedule.pending, (state) => {
    //   state.userResponse.status = "PENDING";
    // });
    // builder.addCase(getSchedule.rejected, (state, action) => {
    //   state.userResponse.message = action.payload?.message ? action.payload.message : "Что-то пошло не так(";
    //   state.userResponse.status = "FAILED";
    // });
    // builder.addCase(getSchedule.fulfilled, (state, action) => {
    //   state.userResponse.status = "SUCCEEDED";
    //   state.user = action.payload.data.user;
    // });

    // builder.addCase(getUsers.pending, (state) => {
    //   state.userResponse.status = "PENDING";
    // });
    // builder.addCase(getUsers.rejected, (state, action) => {
    //   state.userResponse.message = action.payload?.message ? action.payload.message : "Что-то пошло не так(";
    //   state.userResponse.status = "FAILED";
    // });
    // builder.addCase(getUsers.fulfilled, (state, action) => {
    //   state.usersResponse.status = "SUCCEEDED";
    //   state.usersResponse.data = action.payload.data;
    // });

    // builder.addCase(addReceiver.pending, (state) => {
    //   state.editResponse.status = "PENDING";
    // });
    // builder.addCase(addReceiver.rejected, (state, action) => {
    //   state.editResponse.message = action.payload?.message ? action.payload.message : "Что-то пошло не так(";
    //   state.editResponse.status = "FAILED";
    // });
    // builder.addCase(addReceiver.fulfilled, (state, action) => {
    //   state.editResponse.status = "SUCCEEDED";
    //   state.usersResponse.data = action.payload.data;
    // });

    // builder.addCase(addManager.pending, (state) => {
    //   state.editResponse.status = "PENDING";
    // });
    // builder.addCase(addManager.rejected, (state, action) => {
    //   state.editResponse.message = action.payload?.message ? action.payload.message : "Что-то пошло не так(";
    //   state.editResponse.status = "FAILED";
    // });
    // builder.addCase(addManager.fulfilled, (state, action) => {
    //   state.editResponse.status = "SUCCEEDED";
    //   state.usersResponse.data = action.payload.data;
    // });

    // builder.addCase(editUser.pending, (state) => {
    //   state.editResponse.status = "PENDING";
    // });
    // builder.addCase(editUser.rejected, (state, action) => {
    //   state.editResponse.message = action.payload?.message ? action.payload.message : "Что-то пошло не так(";
    //   state.editResponse.status = "FAILED";
    // });
    // builder.addCase(editUser.fulfilled, (state, action) => {
    //   state.editResponse.status = "SUCCEEDED";
    //   state.usersResponse.data = action.payload.data;
    // });

    // builder.addCase(deleteUser.pending, (state) => {
    //   state.editResponse.status = "PENDING";
    // });
    // builder.addCase(deleteUser.rejected, (state, action) => {
    //   state.editResponse.message = action.payload?.message ? action.payload.message : "Что-то пошло не так(";
    //   state.editResponse.status = "FAILED";
    // });
    // builder.addCase(deleteUser.fulfilled, (state, action) => {
    //   state.editResponse.status = "SUCCEEDED";
    //   state.usersResponse.data = action.payload.data;
    // });
  },
});

export const productsReducer = slice.reducer;
export const productsActions = slice.actions;