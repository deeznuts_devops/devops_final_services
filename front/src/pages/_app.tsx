import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { store } from '@/store';
import { Provider as StoreProvider } from 'react-redux';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <StoreProvider store={store}>
      <Component {...pageProps} />
    </StoreProvider>
  )
}
