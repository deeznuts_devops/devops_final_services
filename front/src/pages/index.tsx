import * as React from "react"
import { useAppSelector, useAppDispatch } from "@/hooks/useRedux"
import { Inter } from 'next/font/google'

import { BasketDialog } from "@/components/BasketDialog"
import { ShoppingCartButton } from "@/components/ShoppingCartButton"
import { ProductCard } from "@/components/ProductCard"


const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  const products = useAppSelector((state) => state.products);
  const basket = useAppSelector((state) => state.basket);
  return (
    <main className={`${inter.className} p-2`}>
      <div className="text-xl font-bold">Список продуктов</div>
      <ShoppingCartButton />
      <BasketDialog />
      <div className="grid justify-items-center grid-cols-1 sm:grid-cols-2 min-[1000px]:grid-cols-3 2xl:grid-cols-4 gap-4">
        {products.products?.map((product, index) => {
          const inBasket = basket.items.find((item) => item.product.id === product.id);
          return (
            <ProductCard key={index} product={product} inBasket={inBasket} />
          )
        })}
      </div>
    </main>
  )
}
