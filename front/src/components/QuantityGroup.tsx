import React from 'react'
import { Button } from './ui/button'
import { TBasketItem } from '@/models/Product'
import { useAppDispatch, useAppSelector } from '@/hooks/useRedux'
import { basketActions } from '@/store/basket/slice';

interface IProps {
  item: TBasketItem;
}

export const QuantityGroup: React.FC<IProps> = ({ item }) => {
  const dispatch = useAppDispatch();
  return (
    <div className="flex items-center gap-2">
      <Button variant="outline" onClick={() => dispatch(basketActions.removeItem(item.product.id))}>
        -
      </Button>
      <div className="py-1 px-2 min-w-[2.2rem] text-center">
        {item.quantity}
      </div>
      <Button disabled={item.quantity >= 10} onClick={() => dispatch(basketActions.addItem(item.product))}>
        +
      </Button>
    </div>
  )
}
