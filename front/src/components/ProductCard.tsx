import React from 'react'
import { useAppDispatch } from "@/hooks/useRedux"
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"

import { Button } from '@/components/ui/button'
import { Badge } from "@/components/ui/badge"

import { basketActions } from "@/store/basket/slice"

import Image from 'next/image'
import { ShoppingBagIcon } from "lucide-react"
import { TBasketItem, TProduct } from '@/models/Product'
import { QuantityGroup } from './QuantityGroup'

interface IProps {
  product: TProduct;
  inBasket?: TBasketItem;
}

export const ProductCard: React.FC<IProps> = ({ product, inBasket }) => {
  const dispatch = useAppDispatch();
  return (
    <Card className="flex-1 max-w-sm relative flex flex-col" key={product.id}>
      <CardHeader>
        <div className="w-full h-44 relative rounded-xl overflow-hidden">
          <Image fill objectFit="cover" src={product.photo} alt={product.name} />
        </div>
        <div className="px-6 py-2">
          <CardTitle className="mb-2">{product.name}</CardTitle>
          <div className="flex gap-2 overflow-scroll">
            {product.categories.map((category, index) => (
              <Badge className="whitespace-nowrap" key={index} variant="secondary">{category}</Badge>
            ))}
          </div>
        </div>
      </CardHeader>
      <CardContent>
        <CardDescription className="line-clamp-6">
          {product.description}
        </CardDescription>
      </CardContent>
      <CardFooter className="flex flex-col items-start sm:items-center sm:flex-row sm:justify-between gap-4 flex-wrap mt-auto">
      {inBasket ? (
        <>
          <div>
            <span className="text-xl">${inBasket.quantity * inBasket.product.price}</span>
            {inBasket.quantity > 1 && (
              <span className="text-sm opacity-80"> / ${inBasket.product.price}</span>
            )}
          </div>
          <QuantityGroup item={inBasket} />
        </>
      ) : (
        <>
          <div className="text-xl">${product.price}</div>
          <Button onClick={() => dispatch(basketActions.addItem(product))}>
            <ShoppingBagIcon className="mr-2 h-4 w-4" />
            Добавить в корзину
          </Button>
        </>
      )}
      </CardFooter>
    </Card>
  )
}
