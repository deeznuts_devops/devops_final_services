export type Status = 'IDLE' | 'PENDING' | 'SUCCEEDED' | 'FAILED';

export type TResponse = {
    status: Status,
    message?: string;
}