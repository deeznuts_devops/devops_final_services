![Excalidraw screen](service_struct.png)

# Project Name

Etl script

## Installation

Follow these steps to install and run the ETL script:

### Prerequisites

Make sure you have the following tools installed on your machine:

-   Docker: [Install Docker](https://docs.docker.com/get-docker/)
-   Python: [Install Python](https://www.python.org/downloads/)

### Clone the Repository

```bash
git clone https://github.com/deeznuts_devops/devops_final_services
cd devops_final_services/etl
```

### Set Up Docker Container

Build the Docker image:

```bash
docker build -t etl .
```

### Install Python Dependencies

Create a virtual environment (optional but recommended):

```bash
python -m venv venv
source venv/bin/activate  # On Windows, use `venv\Scripts\activate`
```

Install Python dependencies:

```bash
pip install -r requirements.txt
```

### Configure Environment Variables

Edit the `.env` file and configure any necessary environment variables.

```bash
cp .env.example .env
```

### Run the ETL Script

Execute the ETL script:

```bash
python etl.py
```

### Using Docker

Run the ETL script within the Docker container:

```bash
docker run -it --rm etl python etl.py
```

## TODO

This section outlines the immediate tasks to address in the ETL service:

### General Tasks

-   [ ] **Documentation Update:** Ensure that the README file is up-to-date with the latest information regarding installation, usage, and contribution guidelines.

### Feature Enhancement

-   [ ] **Parallel Processing:** Explore options for introducing parallel processing to improve the ETL script's performance.
-   [ ] **Logging Improvement:** Enhance logging within the ETL script to provide better visibility into the execution flow and potential errors.

## Contributing

If you would like to contribute to the project, please follow our [Contributing Guidelines](CONTRIBUTING.md).

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
