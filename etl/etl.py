from apscheduler.schedulers.blocking import BlockingScheduler
from datetime import datetime, timedelta
import psycopg2
import os
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

# Database credentials
DB_NAME = os.getenv("DB_NAME")
DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_HOST = os.getenv("DB_HOST")
DB_PORT = os.getenv("DB_PORT")

# ETL interval in minutes
ETL_INTERVAL_MINUTES = int(os.getenv("ETL_INTERVAL_MINUTES", 1))

def etl_job():
    conn = psycopg2.connect(
        dbname=DB_NAME,
        user=DB_USER,
        password=DB_PASSWORD,
        host=DB_HOST,
        port=DB_PORT
    )
    cursor = conn.cursor()

    # Calculate the time threshold for the query
    threshold_time = datetime.utcnow() - timedelta(minutes=ETL_INTERVAL_MINUTES)

    query = """
    SELECT COUNT(*), SUM(amount) FROM purchases
    WHERE purchase_time >= %s;
    """

    cursor.execute(query, (threshold_time,))
    results = cursor.fetchone()

    print(
        f"Purchases in the last {ETL_INTERVAL_MINUTES} minute(s): {results[0]}, Total amount: {results[1]}")

    # Remember to commit the transaction if you make any changes to the database
    # conn.commit()

    # Close the connection
    cursor.close()
    conn.close()

scheduler = BlockingScheduler()
scheduler.add_job(etl_job, 'interval', minutes=ETL_INTERVAL_MINUTES)

scheduler.start()
